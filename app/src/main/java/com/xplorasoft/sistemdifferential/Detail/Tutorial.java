package com.xplorasoft.sistemdifferential.Detail;

import com.stephentuso.welcome.FullscreenParallaxPage;
import com.stephentuso.welcome.WelcomeActivity;
import com.stephentuso.welcome.WelcomeConfiguration;

import com.xplorasoft.sistemdifferential.R;

public class Tutorial extends WelcomeActivity {
    @Override
    protected WelcomeConfiguration configuration() {
        return new WelcomeConfiguration.Builder(this)
                .defaultBackgroundColor(R.color.white)
                .page(new FullscreenParallaxPage( R.layout.tutorial1))
                .page(new FullscreenParallaxPage( R.layout.tutorial2))
                .page(new FullscreenParallaxPage( R.layout.tutorial3))
                .page(new FullscreenParallaxPage( R.layout.tutorial4))
                .swipeToDismiss(false)
                .build();
    }
}
