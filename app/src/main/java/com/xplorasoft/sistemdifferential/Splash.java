package com.xplorasoft.sistemdifferential;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.ads.MobileAds;

public class Splash extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);

        imageView = findViewById(R.id.image);

        MobileAds.initialize(this, ""+getString(R.string.admob_app_id));

        playAnimation();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                transition();
            }
        },2000);

    }

    public void playAnimation()
    {
        YoYo.with(Techniques.Bounce)
                .duration(2000)
                .playOn(imageView);
    }

    public void transition()
    {
        Intent splash2 = new Intent(Splash.this, MainActivity.class);
        startActivity(splash2);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
