package com.xplorasoft.sistemdifferential.Home;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import com.xplorasoft.sistemdifferential.Database.Data_Cache;
import com.xplorasoft.sistemdifferential.Detail.Evaluasi_Soal;
import com.xplorasoft.sistemdifferential.R;

public class Evaluasi extends AppCompatActivity {

    Button mulaiEvaluasi;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_menu5_evaluasi);


        mulaiEvaluasi = findViewById(R.id.mulaiEvaluasi);
        mulaiEvaluasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Evaluasi.this, Evaluasi_Soal.class);
                startActivity(intent);
            }
        });

        Data_Cache data_cache = Data_Cache.findById(Data_Cache.class,1L);
        TextView skor = findViewById(R.id.skor);
        skor.setText("Skor Tertinggi : "+data_cache.skorku);
    }

}
