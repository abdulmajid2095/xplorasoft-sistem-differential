package com.xplorasoft.sistemdifferential.Home;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.otaliastudios.zoom.ZoomImageView;

import me.biubiubiu.justifytext.library.JustifyTextView;
import com.xplorasoft.sistemdifferential.R;

public class CaraKerja extends AppCompatActivity {

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_cara_kerja);

        AdView mAdView = findViewById(R.id.adView);
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest adRequest = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();
        mAdView.loadAd(adRequest);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        JustifyTextView text1 = findViewById(R.id.teks1);
        text1.setText("          Putaran poros engkol dari mesin melalui transmisi oleh propeller shaft diperkecil  sesuai  tenaga  yang  diteruskan  oleh  drive  pinion  ke  ring  gear, sebaliknya momennya bertambah maka arah transmisi berubah terhadap arah semula. Pada differential case terdapat dua roda gigi pinion (pinion gear) dan side gear, sehingga bila differential case berputar   maka poros pinion (pinion shaft) ikut berputar yang menyebabkan side gear juga berputar. Side gear dihubungkan ke poros roda belakang dan memindahkan tenaga putar ke roda. Putaran pada poros menjadi rendah karena tenaga putar pada propeller shaft   telah direduksi oleh drive pinion yang berkaitan dengan ring gear yang konstruksinya lebih banyak.                                    ");

        JustifyTextView text2 = findViewById(R.id.teks2);
        text2.setText("        Tekanan kedua roda pada saat berjalan roda penggerak hampir sama pada saat kendaraan berjalan lurus dengan jalan datar. Pada kedua side gear berputar   sebanding   dengan   putaran   differential   pinion   dan   semua komponen berputar dalam satu unit. Apabila tekanan kedua roda belakang sama differential pinion tidak berputar sendiri tetapi berputar bersama ring gear, differential case, poros pinion. Dengan demikian differential pinion hanya berfungsi sebagai penghubung antara side gear kiri dan kanan, sehingga side gear berputar dalam satu unit dengan putaran differential pinion yang menyebabkan kedua poros roda berputar pada kecepatan yang sama.                                    ");

        final ZoomImageView zoomImage = findViewById(R.id.zoom_image);
        Drawable img = getResources().getDrawable( R.drawable.cara1);
        zoomImage.setImageDrawable(img);

        final ZoomImageView zoomImage2 = findViewById(R.id.zoom_image2);
        Drawable img2 = getResources().getDrawable( R.drawable.cara2);
        zoomImage2.setImageDrawable(img2);

        JustifyTextView text3 = findViewById(R.id.teks3);
        text3.setText("        Pada saat kendaraan sedang membelok beban yang ditanggung pada roda bagian dalam adalah lebih besar dari pada beban yang ditanggung roda bagian luar. Apabila kendaraan belok kanan, jarak tempuh roda kiri lebih panjang dibanding jarak tempuh roda kanan, bila dibandingkan kendaraan berjalan lurus. Pada saat kendaraan belok kanan side gear bagian kanan tertahan, differential pinion   berputar masing-masing porosnya dan bergerak mengelilingi axel shaft, akibatnya putaran side gear kiri bertambah cepat.                                          ");

        final ZoomImageView zoomImage3 = findViewById(R.id.zoom_image3);
        Drawable img3 = getResources().getDrawable( R.drawable.cara3);
        zoomImage3.setImageDrawable(img3);

        final ZoomImageView zoomImage4 = findViewById(R.id.zoom_image4);
        Drawable img4 = getResources().getDrawable( R.drawable.cara4);
        zoomImage4.setImageDrawable(img4);

        JustifyTextView text4 = findViewById(R.id.teks4);
        text4.setText("      Sebaliknya apabila kendaraan berbelok ke kiri, jarak tempuh roda kanan lebih jauh dengan  jarak  tempuh roda kiri bila dibandingkan  pada saat kendaraan  berjalan  lurus.  Pada  saat  belok  kiri,  tiap  differential  pinion berputar melalui masing-masing porosnya serta bergerak mengelilingi axel shaft, akibatnya putaran side gear kanan bertambah cepat.                                           ");

        final ZoomImageView zoomImage5 = findViewById(R.id.zoom_image5);
        Drawable img5 = getResources().getDrawable( R.drawable.cara5);
        zoomImage5.setImageDrawable(img5);

        final ZoomImageView zoomImage6 = findViewById(R.id.zoom_image6);
        Drawable img6 = getResources().getDrawable( R.drawable.cara6);
        zoomImage6.setImageDrawable(img6);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
