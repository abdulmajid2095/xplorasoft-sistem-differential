package com.xplorasoft.sistemdifferential.Home;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.otaliastudios.zoom.ZoomImageView;

import com.xplorasoft.sistemdifferential.R;

public class PetaMateri extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_peta_materi);

        final ZoomImageView zoomImage = findViewById(R.id.zoom_image);
        Drawable img1 = getResources().getDrawable( R.drawable.materi );
        zoomImage.setImageDrawable(img1);

    }
}
