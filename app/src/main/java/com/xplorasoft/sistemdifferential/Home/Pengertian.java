package com.xplorasoft.sistemdifferential.Home;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.otaliastudios.zoom.ZoomImageView;

import me.biubiubiu.justifytext.library.JustifyTextView;
import com.xplorasoft.sistemdifferential.R;

public class Pengertian extends AppCompatActivity {

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pengertian);

        AdView mAdView = findViewById(R.id.adView);
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest adRequest = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();
        mAdView.loadAd(adRequest);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.arrow_back));

        /*TextView teks1 = findViewById(R.id.teks1);
        teks1.setText(Html.fromHtml("Menurut (Bayu Prasetya Aji, 2008: 5) <i>differential</i> atau sering dikenal dengan nama gardan adalah komponen pada mobil yang berfungsi untuk meneruskan tenaga mesin ke poros roda yang sebelumnya melewati transmisi dan <i>propeller  shaft</i>  sehingga  dapat  memutarkan  roda dan  menjalankan  kendaraan. Putaran roda semuanya berasal dari proses pembakaran yang terjadi dalam ruang bakar. Proses pembakaran inilah yang kemudian akan menggerakkan piston untuk bergerak naik turun. Kemudian gerak naik turun piston ini akan diteruskan untuk memutar poros engkol. Gerak putar poros engkol pada mesin ini akan diteruskan untuk memutar <i>flywheel</i>. Putaran <i>flywheel</i> akan diteruskan untuk memutar kopling kemudian dilanjutkan memutar transmisi ke propeller lalu ke <i>differential</i>."));

        TextView teks2 = findViewById(R.id.teks2);
        teks2.setText(Html.fromHtml("<i>Differential</i> akan meneruskan putaran ini ke poros <i>axel</i> sesuai dengan beban dari kendaraan dan poros <i>axel</i> akan memutar roda, sehingga kendaraan dapat berjalan. Jadi dapat diketahui urutan perpindahan tenaga dan putaran dari mesin sampai ke roda, sehingga kendaraan atau mobil dapat berjalan. Fungsi utama <i>differential</i> adalah membedakan putaran roda kiri dan kanan pada saat mobil sedang membelok. Hal itu dimaksudkan agar mobil dapat membelok dengan baik tanpa membuat kedua ban menjadi slip atau tergelincir. Ukuran dari sebuah <i>differential</i> menggambarkan dari bobot atau berat kendaraan, namun dalam proses pembagian putaran <i>side gear</i> kiri maupun <i>side gear</i> kanan keduanya memiliki kemampuan yang sama."));
*/
        /*JustifyTextView text1 = findViewById(R.id.teks1);
        text1.setText(Html.fromHtml("Menurut (Bayu Prasetya Aji, 2008: 5) <i>differential</i> atau sering dikenal dengan nama gardan adalah komponen pada mobil yang berfungsi untuk meneruskan tenaga mesin ke poros roda yang sebelumnya melewati transmisi dan <i>propeller  shaft</i>  sehingga  dapat  memutarkan  roda dan  menjalankan  kendaraan. Putaran roda semuanya berasal dari proses pembakaran yang terjadi dalam ruang bakar. Proses pembakaran inilah yang kemudian akan menggerakkan piston untuk bergerak naik turun. Kemudian gerak naik turun piston ini akan diteruskan untuk memutar poros engkol. Gerak putar poros engkol pada mesin ini akan diteruskan untuk memutar <i>flywheel</i>. Putaran <i>flywheel</i> akan diteruskan untuk memutar kopling kemudian dilanjutkan memutar transmisi ke propeller lalu ke <i>differential</i>.        ").toString());
        JustifyTextView text2 = findViewById(R.id.teks2);
        text2.setText(Html.fromHtml("<i>Differential</i> akan meneruskan putaran ini ke poros <i>axel</i> sesuai dengan beban dari kendaraan dan poros <i>axel</i> akan memutar roda, sehingga kendaraan dapat berjalan. Jadi dapat diketahui urutan perpindahan tenaga dan putaran dari mesin sampai ke roda, sehingga kendaraan atau mobil dapat berjalan. Fungsi utama <i>differential</i> adalah membedakan putaran roda kiri dan kanan pada saat mobil sedang membelok. Hal itu dimaksudkan agar mobil dapat membelok dengan baik tanpa membuat kedua ban menjadi slip atau tergelincir. Ukuran dari sebuah <i>differential</i> menggambarkan dari bobot atau berat kendaraan, namun dalam proses pembagian putaran <i>side gear</i> kiri maupun <i>side gear</i> kanan keduanya memiliki kemampuan yang sama.             ").toString());*/

        JustifyTextView text1 = findViewById(R.id.teks1);
        text1.setText("      Menurut (Bayu Prasetya Aji, 2008: 5) differential atau sering dikenal dengan nama gardan adalah komponen pada mobil yang berfungsi untuk meneruskan tenaga mesin ke poros roda yang sebelumnya melewati transmisi dan propeller shaft sehingga dapat memutarkan roda dan menjalankan kendaraan. Putaran roda semuanya berasal dari proses pembakaran yang terjadi dalam ruang bakar. Proses pembakaran inilah yang kemudian akan menggerakkan piston untuk bergerak naik turun. Kemudian gerak naik turun piston ini akan diteruskan untuk memutar poros engkol. Gerak putar poros engkol pada mesin ini akan diteruskan untuk memutar flywheel. Putaran flywheel akan diteruskan untuk memutar kopling kemudian dilanjutkan memutar transmisi ke propeller lalu ke differential.                                                      ");
        JustifyTextView text2 = findViewById(R.id.teks2);
        text2.setText("      Differential akan meneruskan putaran ini ke poros axel sesuai dengan beban dari kendaraan dan poros axel akan memutar roda, sehingga kendaraan dapat berjalan. Jadi dapat diketahui urutan perpindahan tenaga dan putaran dari mesin sampai ke roda, sehingga kendaraan atau mobil dapat berjalan. Fungsi utama differential adalah membedakan putaran roda kiri dan kanan pada saat mobil sedang membelok. Hal itu dimaksudkan agar mobil dapat membelok dengan baik tanpa membuat kedua ban menjadi slip atau tergelincir. Ukuran dari sebuah differential menggambarkan dari bobot atau berat kendaraan, namun dalam proses pembagian putaran side gear kiri maupun side gear kanan keduanya memiliki kemampuan yang sama.              ");


        final ZoomImageView zoomImage5 = findViewById(R.id.zoom_image5);
        Drawable img5 = getResources().getDrawable( R.drawable.tambah1);
        zoomImage5.setImageDrawable(img5);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
