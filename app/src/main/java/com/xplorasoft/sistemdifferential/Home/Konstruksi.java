package com.xplorasoft.sistemdifferential.Home;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.otaliastudios.zoom.ZoomImageView;

import me.biubiubiu.justifytext.library.JustifyTextView;
import com.xplorasoft.sistemdifferential.R;

public class Konstruksi extends AppCompatActivity {

    ImageView suara1,suara2,suara3,suara4,suara5,suara6,suara7,suara8,suara9,suara10,suara11,suara12,suara13;
    Boolean aktif1=false,aktif2=false,aktif3=false,aktif4=false,aktif5=false,aktif6=false,aktif7=false,aktif8=false,aktif9=false,aktif10=false,aktif11=false,aktif12=false,aktif13=false;
    Toolbar toolbar;
    MediaPlayer beep,beep2,beep3,beep4,beep5,beep6,beep7,beep8,beep9,beep10,beep11,beep12,beep13;

    DialogPlus dialog;
    MediaPlayer mediaPlayer;

    Boolean isPlaying = true;
    SeekBar seekBar;
    CountDownTimer countDownTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_konstruksi);

        AdView mAdView = findViewById(R.id.adView);
        Bundle extras = new Bundle();
        extras.putString("max_ad_content_rating", "G");
        AdRequest adRequest = new AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                .build();
        mAdView.loadAd(adRequest);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.arrow_back));

        /*JustifyTextView text1 = findViewById(R.id.teks1);
        text1.setText(Html.fromHtml("Differential   terdiri   dari   beberapa   komponen   berikut   ini:   pinion penggerak (drive pinion), poros pinion (differential  pinion shaft), roda gigi cincin (ring gear) atau differential carrier, bantalan-bantalan, mur penyetel bantalan, perapat oli (oil seal), dan poros roda belakang (axel shaft).                     ").toString());*/
        JustifyTextView text1 = findViewById(R.id.teks1);
        text1.setText("      Differential terdiri dari beberapa komponen berikut ini: pinion penggerak (drive pinion), poros pinion (differential  pinion shaft), roda gigi cincin (ring gear) atau differential carrier, bantalan-bantalan, mur penyetel bantalan, perapat oli (oil seal), dan poros roda belakang (axel shaft).                                   ");


        JustifyTextView text2 = findViewById(R.id.teks2);
        text2.setText(Html.fromHtml("Komponen differential adalah sebagai berikut :").toString());

        final ZoomImageView zoomImage = findViewById(R.id.zoom_image);
        Drawable img1 = getResources().getDrawable( R.drawable.kon1 );
        zoomImage.setImageDrawable(img1);

        final ZoomImageView zoomImage2 = findViewById(R.id.zoom_image2);
        Drawable img2 = getResources().getDrawable( R.drawable.kon2);
        zoomImage2.setImageDrawable(img2);

        final ZoomImageView zoomImage3 = findViewById(R.id.zoom_image3);
        Drawable img3 = getResources().getDrawable( R.drawable.kon3);
        zoomImage3.setImageDrawable(img3);

        JustifyTextView text3 = findViewById(R.id.teks3);
        text3.setText("Merupakan pemindah tenaga mesin melalui poros propeller yang diteruskan ke roda belakang melalui gigi differential dan poros axel. Differential mempunyai tiga fungsi sebagai berikut :               ");

        JustifyTextView text4 = findViewById(R.id.teks4);
        text4.setText("Bila kendaraan berubah haluan akan membuat roda belakang bagian dalam berputar lebih lambat dari pada roda belakang bagian luar, sehingga tidak terjadi slip. Cara kerja ini dilakukan oleh gigi differential yang terdiri dari gigi samping (side gear) dan gigi pinion (pinion gear).          ");

        JustifyTextView text5 = findViewById(R.id.teks5);
        text5.setText("          Prinsip dasar dari differential ini digambarkan seperti roda gigi pinion dan dua rack. Dimana rack tersebut dapat menggelincir pada arah vertikal sejauh berat rack dan tahanan gelincir terangkat bersama. Roda gigi pinion diletakkan diantara dua rack dan pinion dihubungkan dengan penyangga dan dapat pula roda gigi pinion digerakkan dengan penyangga tersebut.                                  ");

        final ZoomImageView zoomImage4 = findViewById(R.id.zoom_image4);
        Drawable img4 = getResources().getDrawable( R.drawable.kon4);
        zoomImage4.setImageDrawable(img4);

        JustifyTextView text6 = findViewById(R.id.teks6);
        text6.setText("          Bila beban W yang sama diletakkan pada rack kemudian alat penyangga (shakle) ditarik ke atas maka kedua rack akan terangkat pada jarak yang sama, hal ini  bertujuan  agar  pinion  tidak  berubah  dan  tetap.  Bila  beban  yang  besar diletakkan pada rack sebelah kiri dan penyangga (shakle) ditarik seperti gambar B pinion akan berputar sepanjang gigi rack yang terkena beban lebih berat hal ini disebabkan  adanya  perbedaan  tahanan  yang  diberikan  pada  pinion,  akibatnya beban yang lebih kecil terangkat. Jarak rack yang terangkat sebanding dengan jumlah putaran pinion, dengan kata lain bahwa rack mendapat tahanan yang lebih besar yang tidak bergerak, sementara tahanan yang lebih kecil akan bergerak. Prinsip ini digunakan pada perencanaan roda-roda gigi differential.                     ");


        final ZoomImageView zoomImage5 = findViewById(R.id.zoom_image5);
        Drawable img5 = getResources().getDrawable( R.drawable.tambah1);
        zoomImage5.setImageDrawable(img5);


        JustifyTextView text7 = findViewById(R.id.teks7);
        text7.setText("Bearing cap adalah komponen yang terletak diantara ineres bearing yang mempunyai fungsi sebegai mengunci bantalan dan untuk mengunci differential case ke differential carier.                               ");
        final ZoomImageView zoomImage6 = findViewById(R.id.zoom_image6);
        Drawable img6 = getResources().getDrawable( R.drawable.tambah2);
        zoomImage6.setImageDrawable(img6);

        JustifyTextView text8 = findViewById(R.id.teks8);
        text8.setText("Backlash/inires bearing adalah suatu komponen yang berfungsi untuk mengurangi gesekan pada machine atau komponen-komponen yang bergerak dan saling menekan antara satu dengan lainnya. Inires bearing sendiri mempunyai fungsi untuk mengurangi gesekan, panas dan aus, menahan beban shaft dan machine, menjaga toleransi kekencangan");
        final ZoomImageView zoomImage7 = findViewById(R.id.zoom_image7);
        Drawable img7 = getResources().getDrawable( R.drawable.tambah3);
        zoomImage7.setImageDrawable(img7);

        JustifyTextView text9 = findViewById(R.id.teks9);
        text9.setText("Adjusting adalah suatu komponen differential yang berada diantara backlash yang digunakan untuk menahan backlash dan untuk menyetel differential. Berfungsi untuk mengatur jarak antara drive pinion dan ring gear.                ");
        final ZoomImageView zoomImage8 = findViewById(R.id.zoom_image8);
        Drawable img8 = getResources().getDrawable( R.drawable.tambah4);
        zoomImage8.setImageDrawable(img8);


        JustifyTextView text10 = findViewById(R.id.teks10);
        text10.setText("Lock Adjusting adalah suatu komponen yang berada pada atas bearing cap yang digunakan agar Adjusting tidak berubah. Lock adjusting mempunyai fungsi pengunci adjusting agar tidak bergerak.                          ");
        final ZoomImageView zoomImage9 = findViewById(R.id.zoom_image9);
        Drawable img9 = getResources().getDrawable( R.drawable.tambah5);
        zoomImage9.setImageDrawable(img9);


        JustifyTextView text11 = findViewById(R.id.teks11);
        text11.setText("Plange yoke adalah suatu komponen yang terletak di penutup differential. Komponen ini mempunyai berfungsi untuk memindahkan tenaga putar poros propeller ke Drive Pinion shaft.                                                  ");
        final ZoomImageView zoomImage10 = findViewById(R.id.zoom_image10);
        Drawable img10 = getResources().getDrawable( R.drawable.tambah6);
        zoomImage10.setImageDrawable(img10);

        JustifyTextView text12 = findViewById(R.id.teks12);
        text12.setText("Oil seal letaknya diujung bagian differential carrier yang berfungsi untuk mencegah agar oli tidak habis, jika diketahui adanya rembasan oli pada bagian ini seger untuk menggantinya karena semakin dibiarkan oli akan habis dan menguap sehingga akan terjadi kerusakan pada komponen lainnya. Fungsi dari oil seal menjaga kebocoran pelumas, memberikan batasan cairan supaya tidak tercampur, melapisi permukaan yang tidak rata, komponen tidak cepat rusak.                      ");
        final ZoomImageView zoomImage11 = findViewById(R.id.zoom_image11);
        Drawable img11 = getResources().getDrawable( R.drawable.tambah7);
        zoomImage11.setImageDrawable(img11);

        JustifyTextView text13 = findViewById(R.id.teks13);
        text13.setText("Side bearing adalah komponen yang berada di antara flens penyambung dan oil seal yang digunakan untuk memperlembut putaran agar tidak ada suara berisik pada differential komponen ini mempunyai fungsi untuk memperlancar/memperlembut putaran.");
        final ZoomImageView zoomImage12 = findViewById(R.id.zoom_image12);
        Drawable img12 = getResources().getDrawable( R.drawable.tambah8);
        zoomImage12.setImageDrawable(img12);

        JustifyTextView text14 = findViewById(R.id.teks14);
        text14.setText("Drive pinion biasa dikenal dengan gigi nanas komponen ini berfungsi untuk meneruskan tenaga putar dari propeller shaft yang selanjutnya dipindahkan ke ring gear lalu dirubah arah putarannya sebesar 90 derajat. Kinerja drive pinion ini berkesinambungan dengan differential case karena keduanya berputar bersamaan. Selain itu berfungsi sebagai pemutar ring gear agar mobil dapat berbelok.                              ");
        final ZoomImageView zoomImage13 = findViewById(R.id.zoom_image13);
        Drawable img13 = getResources().getDrawable( R.drawable.tambah9);
        zoomImage13.setImageDrawable(img13);

        JustifyTextView text15 = findViewById(R.id.teks15);
        text15.setText("Komponen yang biasa dikenal dengan gigi matahari mempunyai lekukan gigi sekitar 50-55 untuk mobil sejenis Suzuki Katana fungsinya adalah meneruskan daya dari propeller shaft di perkecil sesuai tenaga yang diteruskan drive pinion ke ring gear untuk merubah arah perputaran roda sebesar 90 derajat. Ring gear berhubungan dengan drive pinion oleh karena itu apabila ada kerusakan harus membeli satu set karena keduanya harus menempel dengan gap yang standart bila hanya salah satu yang diganti maka akan menimbulkan gap yang tidak sama antara lekukan gigi-giginya. Komponen ini berfungsi sebagai penerus putaran dari drive gear ke pinion dan side gear.                   ");
        final ZoomImageView zoomImage14 = findViewById(R.id.zoom_image14);
        Drawable img14 = getResources().getDrawable( R.drawable.tambah10);
        zoomImage14.setImageDrawable(img14);

        JustifyTextView text16 = findViewById(R.id.teks16);
        text16.setText("Pinion shaft adalah komponen yang terletak antara gigi pinion yang digunakan untuk mengunci gigi pinion dan side gear agar tidak lepas pada pemasangan. Komponen ini berfungsi sebagai tempat dudukan pinion gear.");
        final ZoomImageView zoomImage15 = findViewById(R.id.zoom_image15);
        Drawable img15 = getResources().getDrawable( R.drawable.tambah11);
        zoomImage15.setImageDrawable(img15);

        JustifyTextView text17 = findViewById(R.id.teks17);
        text17.setText("Pinion gear adalah komponen yang terletak diantara side gear Komponen ini berfungsi membedakan putaran side gear kiri dan kanan saat kendaraan berbelok dan washer berfungsi sebagai celah oli. Komponen ini terletak di antara side gear.                ");
        final ZoomImageView zoomImage16 = findViewById(R.id.zoom_image16);
        Drawable img16 = getResources().getDrawable( R.drawable.tambah12);
        zoomImage16.setImageDrawable(img16);

        JustifyTextView text18 = findViewById(R.id.teks18);
        text18.setText("Dapat menghubungkan daya dari drive pinion ke gear terus ke differential pinion lalu ke axle shaft roda belakang, gear inilah yang langsung terhubung ke as roda, jumlahnya ada dua kanan dan kiri. Side gear berfungsi membedakan putaran roda kanan dan kiri saat kendaraan membelok, serta menyeimbangkan kedua roda pada RPM  yang sama pada saat mobil tidak membelok sehingga side gear tetap ikut berputar. Jadi apabila differential case berputar satu  kali, maka side gear juga berputar satu kali juga, demikian seterusnya dalam keadaan lurus. Putaran side gear ini kemudian akan diteruskan untuk menggerakan as roda dan kemudian menggerakan roda. Komponen ini berfungsi meneruskan putaran dari pinion gear ke axle shaft.                  ");
        final ZoomImageView zoomImage17 = findViewById(R.id.zoom_image17);
        Drawable img17 = getResources().getDrawable( R.drawable.tambah13);
        zoomImage17.setImageDrawable(img17);

        JustifyTextView text19 = findViewById(R.id.teks19);
        text19.setText("Differential case berfungsi mengubah arah putaran propeller shaft 90 derajat yang akan diteruskan ke poros roda belakang. Dan juga berfungsi sebagai yang membedakan putaran roda kiri dan kanan pada saat diperlukan. Dengan berputarnya differential case, pinion gear akan terbawa berputar bersama differential case karena antara differential case dan pinion gear dihubungkan dengan pinion shaft. Penyetelan terhadap sistem ini dengan jarak kerenggangan antara ring gear dan drive pinion tidak boleh terlalu rapat atau renggang, jika terlalu rapat akan mengakibatkan berat dan jika terlalu renggang akan menimbulkan suara yang berisik.                             ");
        final ZoomImageView zoomImage18 = findViewById(R.id.zoom_image18);
        Drawable img18 = getResources().getDrawable( R.drawable.tambah14);
        zoomImage18.setImageDrawable(img18);


        suara1 = findViewById(R.id.suara1);
        suara1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif1 == false)
                {
                    aktif1 = true;
                    beep = MediaPlayer.create(getApplicationContext(), R.raw.suara1);
                    beep.setVolume(1, 1);
                    beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep.release();
                            aktif1 = false;
                        }
                    });
                    beep.start();
                }
                else
                {
                    aktif1 = false;
                    beep.stop();
                    beep.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara1);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });

        suara2 = findViewById(R.id.suara2);
        suara2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif2 == false)
                {
                    aktif2 = true;
                    beep2 = MediaPlayer.create(getApplicationContext(), R.raw.suara2);
                    beep2.setVolume(1, 1);
                    beep2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep2.release();
                            aktif2 = false;
                        }
                    });
                    beep2.start();
                }
                else
                {
                    aktif2 = false;
                    beep2.stop();
                    beep2.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara2);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });


        suara3 = findViewById(R.id.suara3);
        suara3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif3 == false)
                {
                    aktif3 = true;
                    beep3 = MediaPlayer.create(getApplicationContext(), R.raw.suara3);
                    beep3.setVolume(1, 1);
                    beep3.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep3.release();
                            aktif3 = false;
                        }
                    });
                    beep3.start();
                }
                else
                {
                    aktif3 = false;
                    beep3.stop();
                    beep3.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara3);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });

        suara4 = findViewById(R.id.suara4);
        suara4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif4 == false)
                {
                    aktif4 = true;
                    beep4 = MediaPlayer.create(getApplicationContext(), R.raw.suara4);
                    beep4.setVolume(1, 1);
                    beep4.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep4.release();
                            aktif4 = false;
                        }
                    });
                    beep4.start();
                }
                else
                {
                    aktif4 = false;
                    beep4.stop();
                    beep4.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara4);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });


        suara5 = findViewById(R.id.suara5);
        suara5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif5 == false)
                {
                    aktif5 = true;
                    beep5 = MediaPlayer.create(getApplicationContext(), R.raw.suara5);
                    beep5.setVolume(1, 1);
                    beep5.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep5.release();
                            aktif5 = false;
                        }
                    });
                    beep5.start();
                }
                else
                {
                    aktif5 = false;
                    beep5.stop();
                    beep5.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara5);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });


        suara6 = findViewById(R.id.suara6);
        suara6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif6 == false)
                {
                    aktif6 = true;
                    beep6 = MediaPlayer.create(getApplicationContext(), R.raw.suara6);
                    beep6.setVolume(1, 1);
                    beep6.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep6.release();
                            aktif6 = false;
                        }
                    });
                    beep6.start();
                }
                else
                {
                    aktif6 = false;
                    beep6.stop();
                    beep6.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara6);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });

        suara7 = findViewById(R.id.suara7);
        suara7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif7 == false)
                {
                    aktif7 = true;
                    beep7 = MediaPlayer.create(getApplicationContext(), R.raw.suara7);
                    beep7.setVolume(1, 1);
                    beep7.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep7.release();
                            aktif7 = false;
                        }
                    });
                    beep7.start();
                }
                else
                {
                    aktif7 = false;
                    beep7.stop();
                    beep7.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara7);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });

        suara8 = findViewById(R.id.suara8);
        suara8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif8 == false)
                {
                    aktif8 = true;
                    beep8 = MediaPlayer.create(getApplicationContext(), R.raw.suara8);
                    beep8.setVolume(1, 1);
                    beep8.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep8.release();
                            aktif8 = false;
                        }
                    });
                    beep8.start();
                }
                else
                {
                    aktif8 = false;
                    beep8.stop();
                    beep8.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara8);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });


        suara9 = findViewById(R.id.suara9);
        suara9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif9 == false)
                {
                    aktif9 = true;
                    beep9 = MediaPlayer.create(getApplicationContext(), R.raw.suara9);
                    beep9.setVolume(1, 1);
                    beep9.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep9.release();
                            aktif9 = false;
                        }
                    });
                    beep9.start();
                }
                else
                {
                    aktif9 = false;
                    beep9.stop();
                    beep9.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara9);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });

        suara10 = findViewById(R.id.suara10);
        suara10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif10 == false)
                {
                    aktif10 = true;
                    beep10 = MediaPlayer.create(getApplicationContext(), R.raw.suara10);
                    beep10.setVolume(1, 1);
                    beep10.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep10.release();
                            aktif10 = false;
                        }
                    });
                    beep10.start();
                }
                else
                {
                    aktif10 = false;
                    beep10.stop();
                    beep10.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara10);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });

        suara11 = findViewById(R.id.suara11);
        suara11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif11 == false)
                {
                    aktif11 = true;
                    beep11 = MediaPlayer.create(getApplicationContext(), R.raw.suara11);
                    beep11.setVolume(1, 1);
                    beep11.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep11.release();
                            aktif11 = false;
                        }
                    });
                    beep11.start();
                }
                else
                {
                    aktif11 = false;
                    beep11.stop();
                    beep11.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara11);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });

        suara12 = findViewById(R.id.suara12);
        suara12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif12 == false)
                {
                    aktif12 = true;
                    beep12 = MediaPlayer.create(getApplicationContext(), R.raw.suara12);
                    beep12.setVolume(1, 1);
                    beep12.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep12.release();
                            aktif12 = false;
                        }
                    });
                    beep12.start();
                }
                else
                {
                    aktif12 = false;
                    beep12.stop();
                    beep12.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara12);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });

        suara13 = findViewById(R.id.suara13);
        suara13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(aktif13 == false)
                {
                    aktif13 = true;
                    beep13 = MediaPlayer.create(getApplicationContext(), R.raw.suara13);
                    beep13.setVolume(1, 1);
                    beep13.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            beep13.release();
                            aktif13 = false;
                        }
                    });
                    beep13.start();
                }
                else
                {
                    aktif13 = false;
                    beep13.stop();
                    beep13.release();
                }*/
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.suara13);
                mediaPlayer.setVolume(1, 1);
                audio(mediaPlayer);
            }
        });


        /*beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
        beep.setVolume(1, 1);
        beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                beep.release();
                beep = MediaPlayer.create(getApplicationContext(), R.raw.beep2);
                beep.setVolume(1, 1);
                beep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        // TODO Auto-generated method stub
                        beep.release();
                    }
                });
                beep.start();
            }
        });
        beep.start();*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void audio(final MediaPlayer mediaPlayer)
    {
        isPlaying = true;
        dialog = DialogPlus.newDialog(this)
                .setContentHolder(new ViewHolder(R.layout.model_audio))
                .setGravity(Gravity.BOTTOM)
                .setInAnimation(R.anim.slide_in_bottom)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setCancelable(false)
                .create();
        View view = dialog.getHolderView();

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                // TODO Auto-generated method stub
                mediaPlayer.release();
                dialog.dismiss();
            }
        });
        mediaPlayer.start();

        seekBar = view.findViewById(R.id.seekbar);
        int max = mediaPlayer.getDuration();
        seekBar.setMax(max);

        final ImageView play = view.findViewById(R.id.play);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlaying)
                {
                    isPlaying = false;
                    mediaPlayer.pause();
                    play.setImageResource(R.drawable.ic_play);
                }
                else
                {
                    isPlaying = true;
                    mediaPlayer.start();
                    play.setImageResource(R.drawable.ic_pause);
                }

            }
        });

        ImageView close = view.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPlaying = false;
                dialog.dismiss();
                mediaPlayer.stop();
                mediaPlayer.release();
                countDownTimer.cancel();
            }
        });

        countDownTimer = new CountDownTimer(120000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                seekBar.setProgress(mediaPlayer.getCurrentPosition());
            }

            @Override
            public void onFinish() {
                countDownTimer.start();
            }
        };
        countDownTimer.start();

        dialog.show();
    }
}
