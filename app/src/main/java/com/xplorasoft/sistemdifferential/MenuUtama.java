package com.xplorasoft.sistemdifferential;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.xplorasoft.sistemdifferential.Home.CaraKerja;
import com.xplorasoft.sistemdifferential.Home.Evaluasi;
import com.xplorasoft.sistemdifferential.Home.Jobsheet;
import com.xplorasoft.sistemdifferential.Home.Konstruksi;
import com.xplorasoft.sistemdifferential.Home.Mekanisme;
import com.xplorasoft.sistemdifferential.Home.Pengertian;
import com.xplorasoft.sistemdifferential.Home.PetaMateri;
import com.xplorasoft.sistemdifferential.Home.Tentang;
import com.xplorasoft.sistemdifferential.Home.Troubleshooting;

public class MenuUtama extends AppCompatActivity implements View.OnClickListener {

    LinearLayout pengertian, konstruksi, mekanisme, cara, trouble, peta, jobsheet, evaluasi, tentang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_menu_utama);

        pengertian = findViewById(R.id.pengertian);
        konstruksi = findViewById(R.id.konstruksi);
        mekanisme = findViewById(R.id.mekanisme);
        cara = findViewById(R.id.cara);
        trouble = findViewById(R.id.trouble);
        peta = findViewById(R.id.peta);
        jobsheet = findViewById(R.id.jobsheet);
        evaluasi = findViewById(R.id.evaluasi);
        tentang = findViewById(R.id.tentang);

        pengertian.setOnClickListener(this);
        konstruksi.setOnClickListener(this);
        mekanisme.setOnClickListener(this);
        cara.setOnClickListener(this);
        trouble.setOnClickListener(this);
        peta.setOnClickListener(this);
        jobsheet.setOnClickListener(this);
        evaluasi.setOnClickListener(this);
        tentang.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.pengertian:
                Intent intent = new Intent(MenuUtama.this, Pengertian.class);
                startActivity(intent);
                break;

            case R.id.konstruksi:
                Intent intent2 = new Intent(MenuUtama.this, Konstruksi.class);
                startActivity(intent2);
                break;

            case R.id.mekanisme:
                Intent intent3 = new Intent(MenuUtama.this, Mekanisme.class);
                startActivity(intent3);
                break;

            case R.id.cara:
                Intent intent4 = new Intent(MenuUtama.this, CaraKerja.class);
                startActivity(intent4);
                break;

            case R.id.trouble:
                Intent intent5 = new Intent(MenuUtama.this, Troubleshooting.class);
                startActivity(intent5);
                break;

            case R.id.peta:
                Intent intent6 = new Intent(MenuUtama.this, PetaMateri.class);
                startActivity(intent6);
                break;

            case R.id.jobsheet:
                Intent intent7 = new Intent(MenuUtama.this, Jobsheet.class);
                startActivity(intent7);
                break;

            case R.id.evaluasi:
                Intent intent8 = new Intent(MenuUtama.this, Evaluasi.class);
                startActivity(intent8);
                break;

            case R.id.tentang:
                Intent intent9 = new Intent(MenuUtama.this, Tentang.class);
                startActivity(intent9);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent keluar = new Intent(MenuUtama.this, MainActivity.class);
        startActivity(keluar);
        finish();
        moveTaskToBack(true);
    }
}
