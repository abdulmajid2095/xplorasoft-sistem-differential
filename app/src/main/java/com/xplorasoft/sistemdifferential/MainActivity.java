package com.xplorasoft.sistemdifferential;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.orm.SugarContext;
import com.xplorasoft.sistemdifferential.Database.Data_Cache;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        SugarContext.init(this);
        Data_Cache data = Data_Cache.findById(Data_Cache.class,1L);
        if((int)data.count(Data_Cache.class, "", null) == 0) {

            Data_Cache data3 = new Data_Cache("0");
            data3.save();
        }

        Handler handler =  new Handler();
        Runnable myRunnable = new Runnable() {
            public void run() {
                // do something
                Intent intent = new Intent(MainActivity.this,MenuUtama.class);
                startActivity(intent);
            }
        };
        handler.postDelayed(myRunnable,3000);

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
